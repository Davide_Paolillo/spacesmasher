﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Block : MonoBehaviour
{
    [SerializeField] private AudioClip audioClip;
    [SerializeField] private GameObject blockSparklesVFX; // VFX means visual effects

    private GameObject particles;
    private float particlesTTL; // Ttl stays for time to live

    private void Awake()
    {
        Assert.IsNotNull(audioClip);
        particlesTTL = 1.0f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.BALL_TAG) && !this.gameObject.tag.Equals(GlobalVar.UNBREAKABLE_BLOCK_TAG))
        {
            PlayDestroyedBlockSFX();
            Destroy(this.gameObject);
            TriggerSparklesVFX();
        }
    }

    private void PlayDestroyedBlockSFX()
    {
        AudioSource.PlayClipAtPoint(audioClip, this.transform.position);
    }

    private void TriggerSparklesVFX()
    {
        particles = UnityEngine.Object.Instantiate(blockSparklesVFX, this.transform.position, this.transform.rotation); // Instantiating particle when a block is destroyed
        Destroy(particles, particlesTTL);
    }
}
