﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class BreakableBlocks : MonoBehaviour
{
    //[SerializeField] private AudioClip audioClip;
    [SerializeField] private GameObject brokenSprite;

    private GameObject brokenBlock;

    private void Awake()
    {
        //Assert.IsNotNull(audioClip);
        Assert.IsNotNull(brokenSprite);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.BALL_TAG))
        {
            //PlayDamagedBlockSFX();
            Destroy(this.gameObject);
            InstantiateBrokenBlock();
        }
    }

    private void InstantiateBrokenBlock()
    {
        brokenBlock = UnityEngine.Object.Instantiate(brokenSprite, this.transform.position, this.transform.rotation);
        if (this.gameObject.tag.Equals(GlobalVar.MULTIPLIER_TAG))
        {
            brokenBlock.tag = this.gameObject.tag;
        }
    }

    private void PlayDamagedBlockSFX()
    {
        //AudioSource.PlayClipAtPoint(audioClip, this.transform.position);
    }
}
