﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class Ball : MonoBehaviour
{
    [SerializeField] private Paddle paddle;
    [SerializeField] private float initialXForce = 2.0f;
    [SerializeField] private float initialYForce = 15.0f;
    [SerializeField] private AudioClip[] audioClip;
    [SerializeField] private float randomFactor = 0.4f;

    private Vector2 myBallPos;
    private bool isStarted;

    private AudioSource audioSource;
    private AudioClip clip;
    private Rigidbody2D rigidbody2D;

    private void Awake()
    {
        Assert.IsNotNull(paddle);
        Assert.IsNotNull(audioClip);
    }

    void Start()
    {
        myBallPos = new Vector2();
        isStarted = false;
        audioSource = GetComponent<AudioSource>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!isStarted)
        {
            LockBallToPaddle();
            LaunchBallOnMouseClick();
        }
        CheckBallVelocity();
    }

    private void CheckBallVelocity()
    {
        if (rigidbody2D.velocity.y < 0.5f && rigidbody2D.velocity.y > -0.5f)
        {
            rigidbody2D.velocity *= 1.2f;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 velocityTweak = new Vector2(UnityEngine.Random.Range((-randomFactor) , randomFactor), UnityEngine.Random.Range((-randomFactor), randomFactor));
        if (isStarted)
        {
            clip = audioClip[UnityEngine.Random.Range(0, audioClip.Length)];
            audioSource.PlayOneShot(clip);
            rigidbody2D.velocity += velocityTweak;
        }
    }

    private void LaunchBallOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isStarted = true;
            rigidbody2D.velocity = new Vector2(initialXForce, initialYForce); // first velocity that we give, after the ball will relie on his bounce
        }
    }

    private void LockBallToPaddle()
    {
        myBallPos.Set(paddle.transform.position.x, this.transform.position.y);
        this.transform.position = myBallPos;
    }
}
