﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// If the aspect ratio is 4:3, means that if we have 12 units (6 for the positive and 6 for the negative) the width of the screen in unit will be 4:3 = x:12 4 is the proportional width and 3 the proportional height, so the 
// screen width will be x = (4x12)/3 = 16 unity unit
public class Paddle : MonoBehaviour
{
    private int screenHeightUnityUnit;
    private int screenWidthUnityUnit;
    private float realtiveMousePositionInTheScreen; // The position of the mouse relative to our screen width, if the screen is 400 width and the mouse is in position 200 on the x axis, we will get a value of 0.5
    private float unityUnitMousePositionInTheScreen; // The position of the mouse relative to our screen unity unit size
    private Vector2 paddlePos;
    private float paddleBorderLeft;
    private float paddleBorderRight;

    void Start()
    {
        // Initializations
        realtiveMousePositionInTheScreen = 0;
        unityUnitMousePositionInTheScreen = 0;
        paddlePos = new Vector2();
        paddleBorderLeft = 1;
        // Calculations
        screenHeightUnityUnit = ((int)Camera.main.orthographicSize * 2); // becaus ortogtaphic size returns the size only on the positive y axis, if we want all the camera size in unity unit we must multiply by 2 to get even the + and the - part of the y axis
        screenWidthUnityUnit = (Screen.width * screenHeightUnityUnit) / Screen.height; // solved the proportion 4:3 = x : 12, in this way the ratio of the screen is not hardcoded, and the code will work with any resolution
        paddleBorderRight = screenWidthUnityUnit;
        /*Debug.Log(Screen.width); ratio 4
        Debug.Log(Screen.height); ratio 3
        Debug.Log(cameraHeightUnityUnit);
        Debug.Log(cameraWidthUnityUnit);*/
    }

    void Update()
    {
        realtiveMousePositionInTheScreen = Input.mousePosition.x / Screen.width;
        unityUnitMousePositionInTheScreen = realtiveMousePositionInTheScreen * screenWidthUnityUnit; // proportion to see where we are in proportion of our game screen based in unity unit
        paddlePos.Set(unityUnitMousePositionInTheScreen, transform.position.y);
        paddlePos.x = Mathf.Clamp(unityUnitMousePositionInTheScreen, paddleBorderLeft, paddleBorderRight);
        transform.position = paddlePos;
    }
}
