﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleDebug : MonoBehaviour
{
    [SerializeField] private GameObject ball;

    private int screenHeightUnityUnit;
    private int screenWidthUnityUnit;
    private float realtiveMousePositionInTheScreen; // The position of the mouse relative to our screen width, if the screen is 400 width and the mouse is in position 200 on the x axis, we will get a value of 0.5
    private float unityUnitMousePositionInTheScreen; // The position of the mouse relative to our screen unity unit size
    private Vector2 paddlePos;
    private float paddleBorderLeft;
    private float paddleBorderRight;
    private bool debug;
    private Vector2 ballPos;

    void Start()
    {
        // Initializations
        realtiveMousePositionInTheScreen = 0;
        unityUnitMousePositionInTheScreen = 0;
        paddlePos = new Vector2();
        debug = false;
        paddleBorderLeft = 1;
        // Calculations
        screenHeightUnityUnit = ((int)Camera.main.orthographicSize * 2); // becaus ortogtaphic size returns the size only on the positive y axis, if we want all the camera size in unity unit we must multiply by 2 to get even the + and the - part of the y axis
        screenWidthUnityUnit = (Screen.width * screenHeightUnityUnit) / Screen.height; // solved the proportion 4:3 = x : 12, in this way the ratio of the screen is not hardcoded, and the code will work with any resolution
        paddleBorderRight = screenWidthUnityUnit - 1;
        /*Debug.Log(Screen.width); ratio 4
        Debug.Log(Screen.height); ratio 3
        Debug.Log(cameraHeightUnityUnit);
        Debug.Log(cameraWidthUnityUnit);*/
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            debug = true;
        }
        if (!debug)
        {
            realtiveMousePositionInTheScreen = Input.mousePosition.x / Screen.width;
            unityUnitMousePositionInTheScreen = realtiveMousePositionInTheScreen * screenWidthUnityUnit; // proportion to see where we are in proportion of our game screen based in unity unit
            paddlePos.Set(unityUnitMousePositionInTheScreen, transform.position.y);
            paddlePos.x = Mathf.Clamp(unityUnitMousePositionInTheScreen, paddleBorderLeft, paddleBorderRight);
            transform.position = paddlePos;
            ballPos = new Vector2(ball.transform.position.x, ball.transform.position.y);
            ballPos.x = Mathf.Clamp(ballPos.x, 0.0f, screenWidthUnityUnit);
            ballPos.y = Mathf.Clamp(ballPos.y, 0, screenHeightUnityUnit);
            ball.transform.position = ballPos;
        }
        else
        {
            ballPos = new Vector2(ball.transform.position.x, ball.transform.position.y);
            ballPos.x = Mathf.Clamp(ballPos.x, 0.0f, screenWidthUnityUnit+1);
            ballPos.y = Mathf.Clamp(ballPos.y, 0, screenHeightUnityUnit+1);
            ball.transform.position = ballPos;
            paddlePos.Set(ball.transform.position.x, this.transform.position.y);
            transform.position = paddlePos;
        }
    }
}
