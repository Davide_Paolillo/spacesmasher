﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCollider : MonoBehaviour
{
    [SerializeField] private SceneLoader scene;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        scene.LoadNextScene(GlobalVar.GAME_OVER_SCENE);
    }
}
