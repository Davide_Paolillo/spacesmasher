﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    private int score = 0;

    public int Score { get => score; set => score = value; }

    private void Awake()
    {
        int gameManagerCount = FindObjectsOfType<Singleton>().Length;
        if (gameManagerCount > 1)
        {
            this.gameObject.SetActive(false); // cos the destroy is executed at the end of a scene, so to be sure of having only one singleton at a time we insta deactivate it
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void ResetGame()
    {
        Destroy(this.gameObject);
    }
}
