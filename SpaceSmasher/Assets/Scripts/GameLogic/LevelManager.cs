﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// TODO implement a modifier for difficulty when we have less then a total blocks
/// </summary>
public class LevelManager : MonoBehaviour
{
    // Configuration parameter
    [SerializeField] private SceneLoader sceneLoader;
    // Cached reference
    private Singleton singleton;
    private TextMeshProUGUI scoreText;
    private List<GameObject> blocks;
    // State variables
    private int multipliers;
    private int scoreMultiplier;
    private int activeBlocks;

    void Start()
    {
        // Initialization
        blocks = new List<GameObject>();
        scoreText = GameObject.FindGameObjectWithTag(GlobalVar.SCORETEXT_TAG).GetComponent<TextMeshProUGUI>();
        singleton = FindObjectOfType<Singleton>();
        // Assignments
        multipliers = 0;
        scoreMultiplier = 1;
        GetBlockInScene();
        AssignRandomMultiplierBlocks();
        AssignBlockNames();
        activeBlocks = blocks.Count;
    }
    
    private void AssignBlockNames()
    {
        for(int i = 0; i < blocks.ToArray().Length; i++)
        {
            if (blocks[i].tag.Equals(GlobalVar.MULTIPLIER_TAG))
            {
                multipliers++;
                blocks[i].name = GlobalVar.MULTIPLIER_TAG;
            } else
            {
                blocks[i].name = blocks[i].tag + " " + i.ToString();
            }
        }
    }

    void Update()
    {
        UpdateBlockInScene();
        CheckWinCondition();
    }

    private void CheckWinCondition()
    {
        if (blocks.ToArray().Length == 0)
        {
            sceneLoader.LoadNextScene();
        }
    }

    private void UpdateBlockInScene()
    {
        int currenMultipliers = GameObject.FindGameObjectsWithTag(GlobalVar.MULTIPLIER_TAG).Length;
        if (currenMultipliers < multipliers)
        {
            multipliers = currenMultipliers;
            scoreMultiplier += 1;
        }
        blocks.Clear();
        GetBlockInScene();
        UpdateScore();
    }

    private void UpdateScore()
    {
        int points = 0;

        if (blocks.Count < activeBlocks)
        {
            points = activeBlocks - blocks.Count;
            activeBlocks = blocks.Count;
            singleton.Score += (points * scoreMultiplier);
            scoreText.text = singleton.Score.ToString();
        }
    }

    private void GetBlockInScene()
    {
        blocks.AddRange(GameObject.FindGameObjectsWithTag(GlobalVar.BLOCK_TAG));
        blocks.AddRange(GameObject.FindGameObjectsWithTag(GlobalVar.TWO_HIT_BLOCK_TAG));
        blocks.AddRange(GameObject.FindGameObjectsWithTag(GlobalVar.THREE_HIT_BLOCK_TAG));
        blocks.AddRange(GameObject.FindGameObjectsWithTag(GlobalVar.MULTIPLIER_TAG));
    }

    public int CountBLocksInScene()
    {
        return blocks.ToArray().Length;
    }

    private void AssignRandomMultiplierBlocks()
    {
        GameObject multiplier;
        for (int i = 0; i < UnityEngine.Random.Range(0, 4); i++)
        {
            multiplier = blocks[UnityEngine.Random.Range(0, blocks.ToArray().Length)];
            multiplier.tag = GlobalVar.MULTIPLIER_TAG;
        }
    }
}
