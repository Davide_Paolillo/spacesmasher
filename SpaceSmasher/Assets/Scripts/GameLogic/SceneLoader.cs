﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private Singleton singleton;

    private void Start()
    {
        singleton = FindObjectOfType<Singleton>();
    }

    /// <summary>method <c>LoadNextScene()</c> load the the scene with the next index in Build order, if the current scene is the last, the first scene is loaded</summary>
    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nexSceneIndex = currentSceneIndex + 1;
        int lastScneIndex = SceneManager.sceneCountInBuildSettings;
        int firstSceneIndex = 0;
        SceneManager.LoadScene(nexSceneIndex < lastScneIndex ? nexSceneIndex : firstSceneIndex);
    }

    /// <summary>method <c>LoadNextScene(string)</c> load the scene accoring to the string parameter passed.</summary>
    public void LoadNextScene(string nextSceneName)
    {
        SceneManager.LoadScene(nextSceneName);
    }

    /// <summary>method <c>QuitGame()</c> quits the game.</summary>
    public void QuitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        singleton.ResetGame();
        LoadNextScene();
    }
}
