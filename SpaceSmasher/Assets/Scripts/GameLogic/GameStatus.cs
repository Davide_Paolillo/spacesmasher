﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// TODO Implement slow the ball if we hit a special block
/// </summary>
public class GameStatus : MonoBehaviour
{
    [Range(0.01f, 1f)] [SerializeField] private float timeScaleFactor = 0.01f;

    private LevelManager levelManager;
    private int blockInScene;

    // Start is called before the first frame update
    void Start()
    {
        levelManager = FindObjectOfType<LevelManager>();
        Time.timeScale = 1.0f;
        blockInScene = levelManager.CountBLocksInScene();
    }

    // Update is called once per frame
    void Update()
    {
        IncrementTimeScale();
    }

    private void IncrementTimeScale()
    {
        if (blockInScene > levelManager.CountBLocksInScene())
        {
            blockInScene = levelManager.CountBLocksInScene();
            Time.timeScale += timeScaleFactor;
        }
    }
}
