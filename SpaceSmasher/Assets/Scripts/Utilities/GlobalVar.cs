﻿public static class GlobalVar
{
    private static string GAME_OVER = "GameOver";
    private static string STARTING_MENU = "StartingMenu";
    private static string LEVEL_ONE = "LevelOne";
    private static string BALL = "Ball";
    private static string BLOCK = "Block";
    private static string MULTIPLIER = "Multiplier";
    private static string SCORETEXT = "ScoreText";
    private static string UNBREAKABLE_BLOCK = "UnbreakableBlock";
    private static string TWO_HIT_BLOCK = "TwoHitBlock";
    private static string THREE_HIT_BLOCK = "ThreeHitBlock";

    public static string GAME_OVER_SCENE { get => GAME_OVER; }
    public static string STARTING_MENU_SCENE { get => STARTING_MENU; }
    public static string LEVEL_ONE_SCENE { get => LEVEL_ONE; }
    public static string BALL_TAG { get => BALL;}
    public static string BLOCK_TAG { get => BLOCK; }
    public static string MULTIPLIER_TAG { get => MULTIPLIER; }
    public static string SCORETEXT_TAG { get => SCORETEXT; }
    public static string UNBREAKABLE_BLOCK_TAG { get => UNBREAKABLE_BLOCK; }
    public static string TWO_HIT_BLOCK_TAG { get => TWO_HIT_BLOCK; }
    public static string THREE_HIT_BLOCK_TAG { get => THREE_HIT_BLOCK; }
}
