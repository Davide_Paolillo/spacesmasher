﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    private TextMeshProUGUI scoreText;
    private Singleton singleton;

    private void Awake()
    {
        scoreText = GameObject.FindGameObjectWithTag(GlobalVar.SCORETEXT_TAG).GetComponent<TextMeshProUGUI>();
        singleton = FindObjectOfType<Singleton>();
        scoreText.text = singleton.Score.ToString();
    }
}
